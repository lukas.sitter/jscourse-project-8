const button = document.querySelector('button');

const buttonClickHandler = event => {
    console.log(event);
}


// button.onclick = buttonClickHandler;
//
// button.addEventListener('click', buttonClickHandler);
//
// window.addEventListener('scroll', event => {
//     console.log(event);
// })

const form = document.querySelector('form')

form.addEventListener('submit', (event) => {
    event.preventDefault();
    console.log(event)
})

const div = document.querySelector('div')

div.addEventListener('click', (event) => {
    console.log('clicked div', event)
}, false)

button.addEventListener('click', (event) => {
    event.stopPropagation();
    console.log('clicked button', event);
})

const listItems = document.querySelectorAll('li');
const list = document.querySelector('ul');
// listItems.forEach(listItem => {
//     listItem.addEventListener('click', event => {
//         event.target.classList.toggle('highlight')
//     })
// })

list.addEventListener('click', event => {
      event.target.classList.toggle('highlight')
   });

